//? CONTROLLER FILE
const express = require('express');
const APIRouter = express.Router();
const Product = require('../models/products.js');

//? INDEX ROUTE - REQUEST COMES FROM Thumbnail.js FILE ON FRONT-END
APIRouter.get('/', async (req, res) => {
  try {
    const thumbnail = await Product.find({}).sort('id');
    res.status(200).json(thumbnail);
  } catch (error) {
    res.status(400).json(error);
  }
});

module.exports = APIRouter;
