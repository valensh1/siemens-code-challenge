# Siemens Business Code Development Project

## LIVE DEPLOYED WEBSITE ON HEROKU

[ https://siemens-code-challenge.herokuapp.com ](https://siemens-code-challenge.herokuapp.com)

## About

This is a code project that was given to me as part of the interview process at Siemens Digital Industries.

## Technologies Used:

- HTML
- CSS/Sass
- JavaScript
- React.js (front-end)
- Node.js (back-end)
- Express.js (back-end)
- MongoDB/Mongoose (database)
- API/AJAX calls
- Utilizes the MERN stack
- Utilizes MVC framework
- Deployed to Heroku

# INSTRUCTIONS FOR RUNNING APPLICATION

## Clone Repository

1. Clone the repo to your local computer

## Installing NPM and Node Modules

2. In Terminal under the root project folder (siemens-interview-challenge) type in the following:
  
            npm install -g npm
   

   - This installs NPM Package Manager. NPM Package Manager allows user the ability to install different add-ons and middleware that were used in application since most developers don't upload these to code repository sites like GitHub or Bitbucket because there are tons of files and it can take up a bunch of space. They can be easily downloaded quickly from NPM within seconds by following the next steps.

3. In Terminal type in the following: 

            npm i
   

   - This installs add-ons and middleware needed for the back-end server in the application to work. Now repeat this very same process for the front-end of the application under the client folder (instructions for this in next step)

4. Navigate to the client folder in Terminal. Type in the following: 
   
            npm i
   

   - This installs add-ons and middleware needed for the front-end React application to work.

## .env File Set-up

5. Inside Visual Studio (VS) Code create a .env file in the root folder (siemens-interview-challenge). See screenshot below.
   
   ![env file creation](client/src/images/ReadMeImages/ENV_fileCreation.png?raw=true 'env file creation')
   
6. Copy the code from the .env example file into your newly created .env file that was created in step 5 above. See screenshot below.
   
   ![env copy example](client/src/images/ReadMeImages/ENVExample_ENV.png?raw=true 'env copy example')
   
7. Replace the username and password in the line of code just copied into the .env file with either your own username and passcode (requires having a MongoDB account) or **contact Shaun Valentine at valensh1@yahoo.com for access**. See screenshot below.
   
   ![env example](client/src/images/ReadMeImages/ENV_Example.png?raw=true 'env example')
   
   - **If using your own MongoDB account you must seed the database as follows!!!** Go to the models directory and open up the products.js file. Inside the products.js file notice how the lines of code 35 thru 172 are commented out. Uncomment these lines of code and hit save. This will seed the MongoDB database with the data for the application once we start up the application in the next steps below.  
   
  
   ![products file](client/src/images/ReadMeImages/Products_File.png?raw=true 'products file')
   
   

## Starting Application

8.  In Terminal go to the root folder (siemens-interview-challenge) and run the following command:
    
             npm run dev
    
9.  Now go to the client folder in Terminal and type in the following:
    
             npm run start
    
    - If you get an error such as this **'react scripts is not recognized as an internal or external command** then inside the client folder in your Terminal type in the following to fix:
      
          
             npm install react-scripts --save
      
10. Application should be started and running now!!!
    
11. **IMPORTANT!!!!** ----> Go back to step 7 above and comment out lines of code 35 thru 172. FORGETTING TO DO THIS STEP WILL RESULT IN DATABASE POPULATING WITH DUPLICATE DATA!!!!

## Using Application

- See screenshot below for instructions.

![application instructions](client/src/images/ReadMeImages/Application_Instructions.png?raw=true 'application instructions')
