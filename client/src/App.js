import './App.scss';
import Thumbnail from './views/Thumbnail.js';

function App() {
  return (
    <div className="App">
      <Thumbnail />
    </div>
  );
}

export default App;
