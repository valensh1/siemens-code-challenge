//? Imports
import { useState, useEffect } from 'react';

//? Global Variables
const displayCountOfThumbnails = 4; // How many thumbnails to display at a time
let beginThumbnail = 0; // Variable to set the beginning thumbnail index position to display on page
let endThumbnail = 4 - 1; // Ending thumbnail index position to display on page; Display 4 thumbnails at a time. Less one is to account for arrays starting at index 0. So to grab first 4 thumbnails would be index position 0 to 3.

const Thumbnail = () => {
  // ALL THUMBNAILS RETRIEVED FROM BACKEND
  const [allThumbnails, setAllThumbnails] = useState([]);

  // CURRENT DISPLAYED THUMBNAILS ON PAGE
  const [displayThumbnails, setDisplayThumbnails] = useState([]);

  // LARGE THUMBNAIL DISPLAYED ON PAGE WITH THUMBNAIL PRODUCT INFORMATION
  const [displayLargeThumbnail, setDisplayLargeThumbnail] = useState({
    title: 'Business Site Template - 7111',
    cost: 45,
    id: '7111',
    description:
      'Lorem ipsum dolor sit amet, dictum et quisque aliquet malesuada at, rutrum ac nullam, elit massa facilisis',
    thumbnail: '7111-m.jpg',
    image: '7111-b.jpg',
  });

  // UPON PAGE LOAD FETCH THE THUMBNAILS OF TEMPLATE PRODUCTS FROM BACKEND
  useEffect(() => {
    (async () => {
      try {
        const response = await fetch('/api/products'); // GET request to backend endpoint
        const data = await response.json(); // Conversion of json data retrieved from backend to JavaScript object to be used in the front-end of application
        await setAllThumbnails(data); // Sets allThumbnails useState variable to the total data set we retrieved from backend which will be used to extract information from and display to user
        await setDisplayThumbnails(data.slice(0, displayCountOfThumbnails)); // Sets our beginning thumbnails to be displayed on page because displayThumbnails useState variable is mapped over in JSX below and displayed to user. In this application it is our first 4 thumbnails.
      } catch (err) {
        console.error(err);
      }
    })();
  }, []);

  // PREVIOUS BUTTON FUNCTIONALITY
  const handleClickPrevious = event => {
    event.preventDefault();
    if (beginThumbnail === 0) return; // If the ending thumbnail index position is greater than or equal to the total length of the AllThumbnails array then that means you are at the end of the filmstrip of thumbnails and just return which will exit the function.
    document.querySelector('.next').classList.remove('disabled'); // When click on previous button this makes the next button active by setting the opacity to 1 so it is no longer grayed out.
    beginThumbnail -= displayCountOfThumbnails; // Calcs the beginning index position of thumbnails to display. With each click it adds displayCountOfThumbnails (which is 4) to the beginning index position since we are displaying only 4 thumbnails each time.
    if (beginThumbnail === 0) {
      document.querySelector('.previous').classList.add('disabled'); // When click on next button this makes the previous button active by setting the opacity to 1 so it is no longer grayed out.
    }
    endThumbnail = endThumbnail - displayCountOfThumbnails; // Calcs the ending index position of thumbnails to display. With each click it adds displayCountOfThumbnails (which is 4) to the beginning index position to be displayed to ensure we are displaying the next set of 4 thumbnails starting with the beginning index position.
    setDisplayThumbnails(allThumbnails.slice(beginThumbnail, endThumbnail)); // Sets our useState of displayThumbnails with the beginning and ending index positions of thumbnails to display which we calc'd above. This useState variable is ultimately mapped over in our JSX below and displayed on page to user in sets of 4 thumbnails except when you reach the end it will only display the remaining thumbnails.
  };

  // NEXT BUTTON FUNCTIONALITY
  const handleClickNext = event => {
    event.preventDefault(); // Prevents page from reloading
    if (endThumbnail >= allThumbnails.length) return; // If the ending thumbnail index position is greater than or equal to the total length of the AllThumbnails array then that means you are at the end of the filmstrip of thumbnails and just return which will exit the function.
    beginThumbnail += displayCountOfThumbnails; // Calcs the beginning index position of thumbnails to display. With each click it adds displayCountOfThumbnails (which is 4) to the beginning index position since we are displaying only 4 thumbnails each time.
    if (beginThumbnail !== 0) {
      document.querySelector('.previous').classList.remove('disabled'); // When click on next button this makes the previous button active by setting the opacity to 1 so it is no longer grayed out.
    }
    endThumbnail = beginThumbnail + displayCountOfThumbnails; // Calcs the ending index position of thumbnails to display. With each click it adds displayCountOfThumbnails (which is 4) to the beginning index position to be displayed to ensure we are displaying the next set of 4 thumbnails starting with the beginning index position.
    setDisplayThumbnails(allThumbnails.slice(beginThumbnail, endThumbnail)); // Sets our useState of displayThumbnails with the beginning and ending index positions of thumbnails to display which we calc'd above. This useState variable is ultimately mapped over in our JSX below and displayed on page to user in sets of 4 thumbnails except when you reach the end it will only display the remaining thumbnails.
    if (endThumbnail >= allThumbnails.length)
      document.querySelector('.next').classList.add('disabled'); // When reach the end of the filmstrip of thumbnails indicated by reaching or exceeding the total length of allThumbnails array then add the disabled class to the next button link which will gray out or decrease opacity of button so user will know that there is no next functionality and that they have reached the end of the filmstrip of thumbnails.
  };

  // THUMBNAIL CLICK FUNCTIONALITY - DISPLAYS LARGER IMAGE AND INFORMATION ABOUT THUMBNAIL PRODUCT
  const handleClickThumbnail = event => {
    event.preventDefault();

    // Remove the class name of 'active' from any DOM element so that we don't have a border surrounding any thumbnail (make it so no thumbnails appear active or highlighted) so the user can then select a thumbnail to make it active and have a border around it
    document
      .querySelectorAll('.thumbnail-links')
      .forEach(el => el.classList.remove('active'));

    event.currentTarget.classList.add('active'); // Give the thumbnail that the user selected a class of 'active' so that the thumbnail appears active/selected with a border around it
    const largeThumbnail = event.currentTarget.title; // Gets the title attribute from DOM which holds the thumbnail number to be used to set the large thumbnail along with its product information

    // Find the specific thumbnail and its related information from the array full of thumbnails (allThumbnails) we fetched from our backend. This is then feeding the displayLargeThumbnail useState which in turn displays the large thumbnail and its related information to user on page.
    const findThumbnail = allThumbnails.find(thumbnail => {
      return thumbnail.id === Number(largeThumbnail);
    });
    setDisplayLargeThumbnail(findThumbnail);
  };

  return (
    <div id="container">
      <header>Code Development Project</header>
      <div id="main" role="main">
        <div id="large">
          <div className="group">
            <img
              src={
                displayLargeThumbnail?.id
                  ? require(`../images/large/${displayLargeThumbnail.id}-b.jpg`)
                      .default
                  : ''
              }
              alt="Large"
              width="430"
              height="360"
            />
            <div className="details">
              <p>
                <strong>Title</strong> {displayLargeThumbnail?.title}
              </p>
              <p>
                <strong>Description</strong>{' '}
                {displayLargeThumbnail?.description}
              </p>
              <p>
                <strong>Cost</strong> {`$${displayLargeThumbnail?.cost}`}
              </p>
              <p>
                <strong>ID #</strong> {displayLargeThumbnail?.id}
              </p>
              <p>
                <strong>Thumbnail File</strong>{' '}
                {displayLargeThumbnail?.thumbnail}
              </p>
              <p>
                <strong>Large Image File</strong> {displayLargeThumbnail?.image}
              </p>
            </div>
          </div>
        </div>

        <div className="thumbnails">
          <div className="group">
            {displayThumbnails.map(thumbnail => {
              return (
                <>
                  <a
                    className="thumbnail-links"
                    href="#"
                    title={thumbnail.id}
                    key={thumbnail.id}
                    onClick={handleClickThumbnail}
                  >
                    <img
                      src={
                        require(`../images/thumbnails/${thumbnail.thumbnail}`)
                          .default
                      }
                      alt={`${thumbnail.id}-m`}
                      width="145"
                      height="121"
                    />
                    <span>{thumbnail.id}</span>
                  </a>
                </>
              );
            })}
            <span
              className="previous disabled"
              title="Previous"
              onClick={handleClickPrevious}
            >
              Previous
            </span>
            <a href="#" className="next" title="Next" onClick={handleClickNext}>
              Next
            </a>
          </div>
        </div>
      </div>
      <footer>
        <a href="instructions.pdf">Download PDF Instructions</a>
      </footer>
    </div>
  );
};

export default Thumbnail;
